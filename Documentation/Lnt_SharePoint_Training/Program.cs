﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSOM
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateNewItem();
            //GetDeptInfo();
            //CreateAnnouncementList();
            //GetList();

            Console.Read();
        }

        private static void GetList()
        {
            ClientContext context = new ClientContext(@"http://spsernew3/sites/IT/");
            Web web = context.Site.RootWeb;
            context.Load(web.Lists);
            context.ExecuteQuery();

            foreach (List item in web.Lists)
            {
                Console.WriteLine(item.Title);
            }
        }

        private static void CreateAnnouncementList()
        {
            ClientContext context = new ClientContext(@"http://spsernew3/sites/IT/");
            Web web = context.Site.RootWeb;
            ListCreationInformation listcreation = new ListCreationInformation();
            listcreation.Title = "my team announcements";
            listcreation.TemplateType = (int)ListTemplateType.Announcements;
            List list = web.Lists.Add(listcreation);
            list.Update();
            context.ExecuteQuery();

            Console.WriteLine("list created");

        }

        private static void GetDeptInfo()
        {
            ClientContext context = new ClientContext(@"http://spsernew3/sites/IT/");
            Web web = context.Site.RootWeb;

            List list = web.Lists.GetByTitle("my team announcements");
            context.ExecuteQuery();
            CamlQuery query = CamlQuery.CreateAllItemsQuery();

            ListItemCollection coll = list.GetItems(query);
            context.Load(coll);
            context.ExecuteQuery();

            foreach (ListItem li in coll)
            {
                Console.WriteLine(li["Title"].ToString());
            }
        }

        private static void CreateNewItem()
        {
            ClientContext context = new ClientContext(@"http://spsernew3/sites/IT/");
            Web web = context.Site.RootWeb;
            List list = web.Lists.GetByTitle("my team announcements");

            ListItemCreationInformation listcreation = new ListItemCreationInformation();
            ListItem teamitem = list.AddItem(listcreation);
            teamitem["Title"] = "Item added dynamically...";
            teamitem.Update();
            context.ExecuteQuery();

            Console.WriteLine("Dynamic item created...");

        }
    }
}
