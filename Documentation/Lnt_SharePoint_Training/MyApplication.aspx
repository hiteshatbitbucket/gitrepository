﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyApplication.aspx.cs" Inherits="ApplicationPageDemo.Layouts.ApplicationPageDemo.MyApplication" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script src="/_layouts/15/sp.runtime.js" type="text/javascript"></script>
    <script src="/_layouts/15/sp.js" type="text/javascript"></script>

    <script type="text/javascript">
        var clientContext;
        var web;
        var list;

        function GetTitle() {
            clientContext = new SP.ClientContext("http://spsernew3/sites/IT");
            web = clientContext.get_web();
            client.load(web);

            clientContext.executeQueryAsnc(Function.createDelegate(this, this.success),
                Function.createDelegate(this, this.failure))
        }
        
        function success() {
            document.getElementById("result").innerHTML = web.get_title();
        }

        function failure() {
            document.getElementById("result").innerHTML = "Something went wrong!";
        }
    </script>
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <input type="button" value="Get Title" onclick="GetTitle()"/>

    <p id="result">Result</p>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Application Page
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
My Application Page
</asp:Content>
